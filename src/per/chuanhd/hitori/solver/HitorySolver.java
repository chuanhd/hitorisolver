/**
 * 
 */
package per.chuanhd.hitori.solver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author chuanhd
 *
 */
public class HitorySolver {

	public int size;
	public int[][] matrix;
	public ArrayList<Integer> possiblePainted = new ArrayList<Integer>();
	private int log10 = 0;
    private String numberFormat;
	private ArrayList<Pair> firstRulePairs = new ArrayList<HitorySolver.Pair>();
	private int numberOfVariable = 0;
	private int numberOfClause = 0;
	private ArrayList<ArrayList<Integer>> thirdRulePaths = new ArrayList<>();
	
//	private static final String minisatPath = "/home/chuanhd/Downloads/minisat/core/";
    
    private class Pair {
    	public int x;
    	public int y;
    	public Pair(int _x, int _y) {
    		x = _x;
    		y = _y;
    	}
    	
    	public String toString() {
    		return new StringBuilder().append("(").append(x).append(", ").append(y).append(")").toString();
    	}
    	
    	/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}
    	
    	/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (!(obj instanceof Pair)) {
				return false;
			}
			Pair other = (Pair) obj;
			if (!getOuterType().equals(other.getOuterType())) {
				return false;
			}
			if (x != other.x) {
				return false;
			}
			if (y != other.y) {
				return false;
			}
			return true;
		}

		private HitorySolver getOuterType() {
			return HitorySolver.this;
		}
    }
    
//    private class Path {
//    	public int from;
//    	public int to;
//    	
//    	public Path (int _from, int _to) {
//    		from = _from;
//    		to = _to;
//    	}
//    	
//    	public String toString() {
//    		return new StringBuilder().append(from).append("->").append(to).toString();
//    	}
//    }
    
	public HitorySolver(String filename) {
		try {
			readFile(filename);
		} catch (Exception e) {
			// TODO: handle exception
			
		}
	}

	/**
	 * Read hitori matrix from file
	 * @param filename: name of input file
	 *
	 **/
	public void readFile(String filename) throws IOException {
        // If included in an Eclipse project.
//		ClassLoader loader = HitorySolver.class.getClassLoader();
//		URL stream = loader.getResource("assets/input.txt");
        InputStream stream = HitorySolver.class.getResourceAsStream(filename);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(stream));

        // If in the same directory - Probably in your case...
        // Just comment out the 2 lines above this and uncomment the line
        // that follows.
        //BufferedReader buffer = new BufferedReader(new FileReader(filename));

        String line;
        int row = 0;

        while ((line = buffer.readLine()) != null) {
            String[] vals = line.trim().split("\\s+");

            // Lazy instantiation.
            if (matrix == null) {
                size = vals.length;
                System.out.println("Size: " + size);
                matrix = new int[size][size];
                log10 = (int) Math.floor(Math.log10(size * size)) + 1;
                numberFormat = String.format("%%%dd", log10);
            }

            for (int col = 0; col < size; col++) {
                matrix[row][col] = Integer.parseInt(vals[col]);
            }

            row++;
        }
        
        numberOfVariable = size * size;
    }
	
	/**
	 * Print input matrix
	 * For debug
	 * */
    public String printHitoriMatrix() {
        StringBuffer buff = new StringBuffer();

        if (matrix != null) {
            for (int row = 0; row < size; row++) {
                buff.append(" ");
                for (int col = 0; col < size; col++) {
                    buff.append(String.format(numberFormat,  matrix[row][col]));
                    if (col < size - 1) {
                        buff.append(" | ");
                    }
                }
                if (row < size - 1) {
                    buff.append("\n");
                    for (int col = 0; col < size; col++) {
                        for (int i = 0; i <= log10 + 1; i++) {
                            buff.append("-");
                        }
                        if (col < size - 1) {
                            buff.append("+");
                        }
                    }
                    buff.append("\n");
                } else {
                    buff.append("\n");
                }
            }
        }

        return buff.toString();
    }
    
    /**
     * Get index of all duplicated value in array
     * @param numberList: input array to check
     * */
    private Map<Integer, ArrayList<Integer>> duplicate(int[] numberList) {
    	Map<Integer, ArrayList<Integer>> duplicateIndex = new HashMap<Integer, ArrayList<Integer>>();
//    	final int MAXZIP = 9;
//    	boolean[] bitmap = new boolean[MAXZIP+1];  // Java guarantees init to false
    	ArrayList<Integer> values;
    	for (int i = 0; i < numberList.length; i++) {
    		if (!duplicateIndex.containsKey(numberList[i])) {
    			values = new ArrayList<Integer>();
    			values.add(i);
    			duplicateIndex.put(numberList[i], values);
			} else { // Duplicate here
				values = duplicateIndex.get(numberList[i]);
				values.add(i);
				duplicateIndex.put(numberList[i], values);
			}
		}
    	
    	return  duplicateIndex;
    }
    
    /**
     * Generate first rule for SAT solver
     */
    public String generateFirstRule() {
    	
    	System.out.println("Generate first rule invoked");
    	
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("c Rule 1").append(System.getProperty("line.separator"));
    	
    	Map<Integer, ArrayList<Integer>> duplicateIndexes;
    	ArrayList<Integer> value;
    	ArrayList<Integer> numbers = new ArrayList<Integer>();
    	// Check repeat item in one row
    	for (int i = 0; i < size; i++) {
    		int[] row = new int[size];
    		for (int j = 0; j < size; j++) {
    			row[j] = matrix[i][j];
    		}
    		duplicateIndexes = duplicate(row);
    		
    		for (Integer key : duplicateIndexes.keySet()) {
    			value = duplicateIndexes.get(key);
    			if (value.size() == 1) { // Element appear once
//					System.out.println(new StringBuffer().append(i * size + value.get(0) + 1).append(" 0")); // CNF format start index from 1
//    				sb.append(i * size + value.get(0) + 1).append(" 0").append(System.getProperty("line.separator"));
//    				numberOfClause++;
    			} else { // Duplicated value in row
    				numbers.clear(); // clear all elements
					for (Integer index : value) {
						if (!possiblePainted.contains(i * size + index)) {
							possiblePainted.add(i * size + index);
						}
						numbers.add(i * size + index + 1);
					}
					possiblePairs(numbers);
				}
    			value = null;
    		}
    		numbers.clear();
    		row = null;
    		duplicateIndexes = null;
    	}
    	
    	value = null;
    	duplicateIndexes = null;
    	
    	System.out.print("Write duplicate in row first");
    	writeFirstRule(sb);
    	
    	for (int j = 0; j < size; j++) {
    		int[] col = new int[size];
    		for (int i = 0; i < size; i++) {
    			col[i] = matrix[i][j];
    		}
    		duplicateIndexes = duplicate(col);
    		for (Integer key : duplicateIndexes.keySet()) {
    			value = duplicateIndexes.get(key);
    			if (value.size() == 1) { // Element appear once
    				
    			} else { // Duplicated value in col
    				if (numbers != null) {
						numbers.clear();
					}
    				for (Integer index : value) {
						if (!possiblePainted.contains(index * size + j)) {
							possiblePainted.add(index * size + j);
						}
						numbers.add(index * size + j + 1);
					}
					possiblePairs(numbers);
					// Free memory
					numbers.clear();
				}
    			value = null;
    		}
    		numbers.clear();
    		value = null;
    		col = null;
    		duplicateIndexes.clear();
    		duplicateIndexes = null;
    	}
    	
    	numbers = null;
    	
//    	Pair _generatePair;
//    	while (firstRulePairs.size() > 0) {
//			_generatePair = firstRulePairs.get(0);
//			sb.append("-").append(_generatePair.x).append(" -").append(_generatePair.y).append(" 0").append(System.getProperty("line.separator"));
//			numberOfClause++;
//			firstRulePairs.remove(0);
//			_generatePair = null;
//		}
    	
    	System.out.println("Write duplicate in col");
    	writeFirstRule(sb);
    	
    	for (int i = 0; i < size * size; i++) {
    		if (!possiblePainted.contains(i)) {
    			sb.append(i + 1).append(" 0").append(System.getProperty("line.separator"));
    			numberOfClause++;
			}
		}
    	
    	return sb.toString();
    }
    
    private void writeFirstRule(StringBuilder sb) {
    	Pair _generatePair;
    	while (firstRulePairs.size() > 0) {
			_generatePair = firstRulePairs.get(0);
			sb.append("-").append(_generatePair.x).append(" -").append(_generatePair.y).append(" 0").append(System.getProperty("line.separator"));
			numberOfClause++;
			firstRulePairs.remove(0);
			_generatePair = null;
		}
    }
    
    /**
     * Generate second rule for SAT solver
     * */
    public String generateSecondRule() {
    	
    	System.out.println("Generate second rule invoked");
    	
    	StringBuilder sb = new StringBuilder();
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("c Rule 2").append(System.getProperty("line.separator"));
    	
    	for (int index : possiblePainted) {
    		System.out.println("posibleIndex: " + (index + 1));
    		int row = index / size;
    		int col = index % size;
    		
    		if (col + 1 <= size - 1) { // next right column item
				sb.append(index + 1).append(" ").append(row * size + col + 1 + 1).append(" 0").append(System.getProperty("line.separator"));
				numberOfClause++;
			}
    		if (col - 1 >= 0) {
    			sb.append(index + 1).append(" ").append(row * size + col - 1 + 1).append(" 0").append(System.getProperty("line.separator"));
    			numberOfClause++;
    		}
    		if (row + 1 <= size - 1) {
    			sb.append(index + 1).append(" ").append((row + 1) * size + col + 1).append(" 0").append(System.getProperty("line.separator"));
    			numberOfClause++;
    		}
    		if (row - 1 >= 0) {
    			sb.append(index + 1).append(" ").append((row - 1) * size + col + 1).append(" 0").append(System.getProperty("line.separator"));
    			numberOfClause++;
    		}
		}
    	
    	return sb.toString();
    }
    
    /**
     * Generate third rule for SAT solver
     * */
    public String generateThirdRule(){
    	
    	System.out.println("Generate third rule invoked");
    	
    	StringBuilder sb = new StringBuilder();
    	
    	if (thirdRulePaths == null) {
			thirdRulePaths = new ArrayList<>();
		}
    	
    	// Get all possible paths between two possible painted cell
    	ArrayList<Integer> paths = new ArrayList<>();
    	ArrayList<Pair> checkedPair = new ArrayList<>();
    	ArrayList<Integer> borderCells = new ArrayList<>();
    	for (Integer first : possiblePainted) {
    		// Find cell in border
    		if (first / size == 0 || first / size == size - 1) { // First row and last row
				if (!borderCells.contains(first)) {
					borderCells.add(first);
				}
			}
			if (first % size == 0 || first % size == size - 1) { // First column and last column
				if (!borderCells.contains(first)) {
					borderCells.add(first);
				}
			}
			for (Integer second : possiblePainted) {
				if (first != second) {
					Pair path = new Pair(first, second);
					Pair reversePath = new Pair(second, first);
					if (checkedPair.contains(path) || checkedPair.contains(reversePath)) {
						continue;
					}
//					System.out.println("FIND PATH: " + first + " -> " + second);
					checkedPair.add(path);
					findPath(first, second, paths);
					path = null;
					reversePath = null;
				}
				paths.clear();
			}
		}
		checkedPair = null;
		paths = null;
		
		HashMap<Pair, ArrayList<ArrayList<Integer>>> dictPaths = new HashMap<>();
		Integer start;
		Integer end;
		Pair fromTo;
		ArrayList<ArrayList<Integer>> _allPaths =  new ArrayList<>();
		for (ArrayList<Integer> path : thirdRulePaths) {
			start = path.get(0); // first index
			end = path.get(path.size() - 1); // last index
			fromTo = new Pair(start, end);
			if (dictPaths.containsKey(fromTo)) {
				_allPaths = dictPaths.get(fromTo);
				_allPaths.add(path);
				dictPaths.put(fromTo, new ArrayList<>(_allPaths));
			} else {
				_allPaths = new ArrayList<>();
				_allPaths.add(path);
				dictPaths.put(fromTo, new ArrayList<>(_allPaths));
			}
			// free memory
			_allPaths.clear();
			_allPaths = null;
			fromTo = null;
		}
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("c Rule 3.1").append(System.getProperty("line.separator"));
    	
//    	int shortestSize = 0;
    	for (Entry<Pair, ArrayList<ArrayList<Integer>>> entry : dictPaths.entrySet()) {
    		fromTo = entry.getKey();
    		_allPaths = entry.getValue();
    		Collections.sort(_allPaths, new Comparator<ArrayList<Integer>>() {

				@Override
				public int compare(ArrayList<Integer> o1, ArrayList<Integer> o2) {
					if (o1.size() < o2.size()) {
						return -1;
					} else if (o1.size() > o2.size()) {
						return 1;
					}
					return 0;
				}
			});
//    		System.out.println(_allPaths.get(0).size() + " " + _allPaths.get(_allPaths.size() - 1).size());
//    		shortestSize = _allPaths.get(0).size();
    		if (borderCells.contains(fromTo.x) && borderCells.contains(fromTo.y)) { // Rule 3.1
//    			System.out.println("Path: " + fromTo.toString());
				for (ArrayList<Integer> path : _allPaths) {
//					if (path.size() > shortestSize) {
//						continue;
//					}
					for (int i = 0; i < path.size(); i++) {
						sb.append(path.get(i) + 1).append(" ");
					}
					sb.append("0").append(System.getProperty("line.separator"));
					numberOfClause++;
				}
			}
		}
    	// Free memory
    	borderCells.clear();
    	borderCells = null;
    	
    	sb.append(System.getProperty("line.separator"));
    	sb.append("c Rule 3.2").append(System.getProperty("line.separator"));
    	Pair indexPair;
    	for (Entry<Pair, ArrayList<ArrayList<Integer>>> entry : dictPaths.entrySet()) {
			fromTo = entry.getKey();
			_allPaths = entry.getValue();
			if (_allPaths.size() > 0) {
				if ((fromTo.x == 19 && fromTo.y == 35) || (fromTo.x == 35 && fromTo.y == 19)) {
					System.out.println("Should enable debug");
				}
				for (int i = 1; i < _allPaths.size(); i++) { // Find different path with longest path
					indexPair = findDifferentPath(_allPaths.get(i), _allPaths.get(0));
					if (indexPair.x != -1 && indexPair.y != -1) {
						for (int k = indexPair.x; k < _allPaths.get(i).size() - indexPair.y; k++) {
//							System.out.print(_allPaths.get(i).get(k) + " ");
							sb.append(_allPaths.get(i).get(k) + 1).append(" ");
						}
						for (int l = _allPaths.get(0).size() - indexPair.y - 1 - 1; l > indexPair.x; l--) {
//							System.out.print(_allPaths.get(_allPaths.size() - 1).get(l) + " ");
							sb.append(_allPaths.get(0).get(l) + 1).append(" ");
						}
						sb.append("0").append(System.getProperty("line.separator"));
						numberOfClause++;
					}
				}
			}
		}
    	
    	return sb.toString();
    }
    
    private int topLeftIndex(int index) {
    	int row = index / size;
    	int col = index % size;
    	if (row - 1 >= 0 && col - 1 >= 0) {
			return (row - 1) * size + col - 1;
		}
    	return -1;
    }
    
    private int bottomLeftIndex(int index) {
    	int row = index / size;
    	int col = index % size;
    	if (row + 1 < size && col - 1 >= 0) {
			return (row + 1) * size + col - 1;
		}
    	return -1;
    }
    
    private int topRightIndex(int index) {
    	int row = index / size;
    	int col = index % size;
    	if (row - 1 >= 0 && col + 1 < size) {
			return (row - 1) * size + col + 1;
		}
    	return -1;
    }
    
    private int bottomRightIndex(int index) {
    	int row = index / size;
    	int col = index % size;
    	if (row + 1 < size && col + 1 < size) {
			return (row + 1) * size + col + 1;
		}
    	return -1;
    }
    
    /**
     * Find all path between to index
     * @param start: index of start cell
     * @param end: index of end cell
     * @param paths: array to stored path
     * */
    private void findPath(int start, int end, ArrayList<Integer> paths) {
		
//    	System.out.println("findPath invoked: " + start + " -> " + end);
    	
    	paths.add(start);
//    	checkedPair.add(new Pair(start, end));
    	
    	if (start == end) {
//    		StringBuilder sb = new StringBuilder();
//    		
//    		for (Integer index : paths) {
//				sb.append(index).append(" -> ");
//			}
//    		
//    		System.out.println(sb.toString());
    		
    		thirdRulePaths.add(new ArrayList<>(paths));
    		
    		paths.remove(paths.size() - 1);
    		
			return;
		}
    	
    	
		
		int topLeftIndex = topLeftIndex(start);
		int bottomLeftIndex = bottomLeftIndex(start);
		int topRightIndex = topRightIndex(start);
		int bottomRightIndex = bottomRightIndex(start);
		
		if (!possiblePainted.contains(topLeftIndex) && !possiblePainted.contains(bottomLeftIndex)
				&& !possiblePainted.contains(topRightIndex) && !possiblePainted.contains(bottomRightIndex)) {
			paths.remove(paths.size() - 1);
			return;
		}
		
//		if (paths.contains(topLeftIndex) && 
//				paths.contains(bottomLeftIndex) && 
//				paths.contains(topRightIndex) && 
//				paths.contains(bottomRightIndex)) {
//			System.out.println("Remove: " + start + " lastIndex: " + paths.get(paths.size() - 1));
//			paths.remove(start);
//			return;
//		}
		
		boolean goNext = false;
		
		if (topLeftIndex > -1 && !paths.contains(topLeftIndex) && possiblePainted.contains(topLeftIndex)) {
			goNext = true;
			findPath(topLeftIndex, end, paths);
		}
		
		if (bottomLeftIndex > -1 && !paths.contains(bottomLeftIndex) && possiblePainted.contains(bottomLeftIndex)) {
			goNext = true;
			findPath(bottomLeftIndex, end, paths);
		}
		
		if (topRightIndex > -1 && !paths.contains(topRightIndex) && possiblePainted.contains(topRightIndex)) {
			goNext = true;
			findPath(topRightIndex, end, paths);
		}
		
		if (bottomRightIndex > -1 && !paths.contains(bottomRightIndex) && possiblePainted.contains(bottomRightIndex)) {
			goNext = true;
			findPath(bottomRightIndex, end, paths);
		}
		
		if (!goNext) {
//			System.out.println("Can't go next: " + start);
			paths.remove(paths.size() - 1);
			return;
		}
		
		if (paths.size() > 1) {
			paths.remove(paths.size() - 1);
			return;
		}
    }
    
    /**
     * Find start and end index of different path of two arrays
     * @param first: first array
     * @param second: second array
     * 
     * */
    private Pair findDifferentPath(ArrayList<Integer> first, ArrayList<Integer> second) {
    	Pair result = new Pair(-1, -1);
    	
    	if (first.size() == 0 || second.size() == 0) {
			return result;
		}
    	
//    	System.out.println("first path: " + first);
//    	System.out.println("second path: " + second);
    	
    	int count = Math.min(first.size(), second.size());
    	
    	for (int i = 0; i < count; i++) {
			if (first.get(i) == second.get(i)) {
				result.x = i; // return index from head
			} else {
				break;
			}
		}
    	
    	for (int i = 0; i < count; i++) {
			if (first.get(first.size() - 1 - i) == second.get(second.size() - 1 - i)) {
//				result.y = first.size() < second.size() ? second.size() - 1 - i : first.size() - 1 - i;
				result.y = i; // return index from tail
			} else {
				break;
			}
		}
    	
    	return result;
    }
    
    /**
     * Create input file for SAT solver
     * @param outputFile: output file name
     * */
    public void generateCNFFileInput(String outputFile) {
    	
    	System.out.println("generateCNFFileInput invoked");
    	
    	try {
    		
    		possiblePainted.clear();
    		thirdRulePaths.clear();
    		firstRulePairs.clear();
    		
			// Create file
    		File hitoriInput = new File(outputFile);
    		hitoriInput.getParentFile().mkdirs();
    		hitoriInput.createNewFile();
    		if (hitoriInput.exists() && !hitoriInput.isDirectory()) {
				System.out.println(outputFile + " is exist");
			}
    		FileOutputStream fos = new FileOutputStream(hitoriInput);
    		OutputStreamWriter osw = new OutputStreamWriter(fos);
    		BufferedWriter bw = new BufferedWriter(osw);
    		String firstRule = generateFirstRule();
    		String secondRule = generateSecondRule();
    		String thirdRule = generateThirdRule();
    		bw.write(new StringBuilder("p cnf ").append(numberOfVariable).append(" ").append(numberOfClause).toString());
    		bw.write(firstRule);
    		bw.write(secondRule);
    		bw.write(thirdRule);
//    		genAndWriteThirdRule(bw);
    		bw.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			
		}
    }
        
    /**
     * Get all possible pairs from array and store for firstRulePairs array
     * @param numbers: Array of elements to make pair
     * */
    public void possiblePairs(ArrayList<Integer> numbers) {
    	if (numbers.size() <= 1) {
			return;
		}
    	
    	int first = numbers.get(0);
    	numbers.remove(0);
    	
    	Pair pair;
    	for (int i = 0; i < numbers.size(); i++) {
//			System.out.println("(" + first + ", " + numbers.get(i) + ")");
    		pair = new Pair(first, numbers.get(i));
    		firstRulePairs.add(pair);
    		pair = null;
		}
    	
    	System.out.println(firstRulePairs.size());
    	
    	possiblePairs(numbers);
    }
    
    /**
     * Get all possible pairs from array
     * @param numbers: Array of elements to make pair
     * @param store: Array to store all possible pairs
     * */
    public void possiblePairs(ArrayList<Integer> numbers, ArrayList<Pair> store) {
    	if (numbers.size() <= 1) {
			return;
		}
    	
    	int first = numbers.get(0);
    	numbers.remove(0);
    	
    	Pair pair;
    	for (int i = 0; i < numbers.size(); i++) {
//			System.out.println("(" + first + ", " + numbers.get(i) + ")");
    		pair = new Pair(first, numbers.get(i));
    		store.add(pair);
    		pair = null;
		}
    	
    	possiblePairs(numbers, store);
    }
    
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//TODO: should read from assets
			HitorySolver solver = new HitorySolver("input_6.txt");
			String inputFileName = "HitoriInput_6.txt";
			String outputFileName = "HitoriOutput_6.txt";
			String inputFilePath = new StringBuilder().append(Config.MINISAT_PATH).append(inputFileName).toString();
			String outputFilePath = new StringBuilder().append(Config.MINISAT_PATH).append(outputFileName).toString();
			solver.generateCNFFileInput(inputFilePath);
			File outPutFile = new File(outputFilePath);
			if (outPutFile.exists() && !outPutFile.isDirectory()) {
				System.out.println(outPutFile + " is exist");
				if(outPutFile.delete()){
	    			System.out.println(outPutFile.getName() + " is deleted!");
	    		}else{
	    			System.out.println("Delete operation is failed.");
	    		}

			}
			System.out.println("Start run minisat");
			System.out.println(CommandExecutor.execute(Config.MINISAT_PATH + "minisat " + inputFilePath + " " + outputFilePath));
			System.out.println("Finished run minisat");
			
			System.out.println("Done generated rule");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
